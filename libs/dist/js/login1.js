var loginApp = angular.module("loginApp", ["commonApp"]);
loginApp.controller("loginCtrl", ["$scope", "$http","$location", function($scope, $http,$location) {
	$scope.errorMsg = "";
	$scope.login = function() {
		if ($scope.account == "" || $scope.password == "" || $scope.account == undefined || $scope.password == undefined) {
			$scope.errorMsg = "请输入用户名或密码";
		}else{
			$scope.errorMsg = "";
			$http.post("libs/php/login.php",{username:$scope.account,password:$scope.password}).success(function(_response){
				_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
				console.log(_response);
				if (_response.state) {
					window.location.href = "vue-backstage.html";
				}else{
					$.alert(_response.message);
				};
			})
		};
	}
}]);