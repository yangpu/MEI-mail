var common = common || {};

//common.baseUrl = 'http://119.29.99.176/app/';
common.baseUrl = '';

var commonApp = angular.module('commonApp', []);



//2.dkhttp的工厂模式的函数，判断是否有登录的操作
commonApp.factory('dkHttp', ['$http', function($http) {
    return {
        smartclient: function(_params) {
            //先判断是否
            $http.get('libs/php/getsession.php').success(function(response) {
                if (!response.state) {
                    console.log("dkNav")
                    //window.location.href = 'index.html';
               } else {
                    //否则的话是执行HTTP
                    $http(_params).success(_params.success);
                }
            })
        }
    };
}])


//3.dk-nav的属性指令
commonApp.directive('dkNav', ['$compile', 'dkHttp', function($compile, dkHttp) {
    return {
        restrict: 'A',
        templateUrl: 'userplate.html',

        link: function(scope, iElement, iAttrs) {
            dkHttp.smartclient({
                url: 'libs/php/getmodel.php',
                params: {
                    type: 'register'
                },
                success: function(response) {
                    console.log("获取表头")
                    scope.navs = response[0];
                    // myScroll.refresh();
                }
            })

            dkHttp.smartclient({
                url: 'libs/php/getmodel.php',
                params: {
                    type: 'tuser2'
                },
                success: function(response) {
                    console.log("获取代币")
                    scope.nav = response;
                    // myScroll.refresh();
                }
            })

            dkHttp.smartclient({
                url: 'libs/php/getmodel.php',
                params: {
                    type: 'tuser1'
                },
                success: function(response) {
                    console.log("获取商家信息")
                    scope.nav1 = response;

                    // setTimeout(function() {
                    //     myScroll.refresh();
                    // }, 1000)
                }
            })
        }
    };
}])

commonApp.filter('range', function () {
            return function (array, range) {
                for (var i = 1 ; i <= range; i++) {
                    array.push(i);
                }
                return array;
            }
        })

//6.创建服务
commonApp.config(["$httpProvider", function($httpProvider) {
    $httpProvider.defaults.transformRequest = function(obj) {
        var str = [];
        for (var p in obj) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
        return str.join("&");
    };
    $httpProvider.defaults.headers.post = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    $httpProvider.interceptors.push(function($rootScope, $q) {
        return {
            'request': function(config) {
                //请求执行
                // if (!$('.mask')[0]) {
                //     var _html = '<div class="mask item-hidden"><i class="fa fa-spinner fa-spin"></i></div>';
                //     $(_html).appendTo($('body'));
                // }
                // $('.mask').removeClass('item-hidden');
                if ($('.mask')[0]) {
                    $('.mask').show();
                } else {
                    $('<div class="mask"><i class="fa fa-spinner fa-spin"></i></div>').appendTo('body').show();
                }
                if (config.url.indexOf('.html') < 0 && config.url.indexOf('.txt') < 0 && config.url.indexOf('.woff')) {
                    config.url = common.baseUrl + config.url;
                }
                config.params = $.extend(config.params, {
                    //防止页面缓存
                    '_': Math.random()
                });
                return config || $q.when(config);
            },
            'requestError': function(rejection) {
                //请求出错的时候执行
                return rejection;
            },
            'response': function(response) {
                //响应成功的回调函数
                $('.mask').hide();
                //$('.mask').addClass('item-hidden');
                return response || $q.when(response);
            },
            'responseError': function(response) {
                //响应失败的回调函数
                alert(response.status + ' - ' + response.statusText + '<br/>请求路径：<br/>' + response.config.url, '请求错误');
                // $('.mask').addClass('item-hidden');
                $('.mask').hide();
                return $q.reject(response);
            }
        };
    });
}]);


//3.全局处理每个html的函数
$(function() {
    $(document).on("touchstart", function(evt) {
        if ($(evt.target).is(".topgo")) {
            window.history.back();
        } else if ($(evt.target).is(".toplist")) {
            $(evt.target).next().toggle();
        };;
    })


})

//4.取id的方式；
;
(function($) {

    $.getUrlParam = function(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }
})(jQuery);