document.addEventListener('touchmove', function(e) {
	e.preventDefault();
}, false);

document.ondragstart = function() {
	return false;
}

var indexapp = angular.module('indexapp', ['commonApp']);
var indexcontroller = indexapp.controller('indexcontroller', ['$scope', '$http', function($scope, $http) {
	$http.get('libs/php/getmodel.php', {
		params: {
			type: 'tproduct2'
		}
	}).success(function(_response) {
		_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
		$scope.data1 = _response;
	})

	$http.get('libs/php/index.php?type=' + 6).success(function(_response) {
		_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
		$scope.data = _response;
		setTimeout(function() {
			myScroll.refresh();
		}, 500)
	})

	$scope.load = function() {
		var myScroll = new IScroll('#wrapper', {
			scrollbars: true,
			mouseWheel: true,
			interactiveScrollbars: true,
			click: true,
			probeType: 3,
		});
		window.myScroll = myScroll;

		var opts = {
			flag: false,
			index: 1,
			times: 6, //数据加载的跨度
		}
		myScroll.on('scrollEnd', function() {
			if (this.y <= myScroll.maxScrollY && myScroll.maxScrollY != opts.flag) {
				opts.flag = myScroll.maxScrollY;
				opts.index++;
				$http.get('libs/php/index.php?type=' + opts.index * opts.times).success(function(_response) {
					_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
					$scope.data = _response;
					setTimeout(function() {
						myScroll.refresh();
					}, 500)
				})
			}
		});

		myScroll.on('scroll', function() {
			if (this.y < -200) {
				$("#totop").show();
			} else {
				$("#totop").hide();
			}
		})
	}

}])


$(function() {
	var mySwiper = new Swiper('.swiper-container', {
		loop: true,
		pagination: '.swiper-pagination',
		paginationClickable: true,
		autoplay: 1000, //可选选项，自动滑动
		speed: 300,
		autoplayDisableOnInteraction: false,
	})

	$("body").on("touchstart", function(evt) {
		if ($(evt.target).is(".iconfont")) {
			var icon = $(evt.target);
			if (icon.hasClass('fa-search')) {
				$(".search").css('display', 'table').siblings().hide();
				$("#totop").hide();
				icon.removeClass("fa-search").addClass("fa-close");
				myScroll.refresh();
			} else if (icon.hasClass('fa-close')) {
				$(".search").hide().siblings().show();
				icon.removeClass('fa-close').addClass("fa-search");
				myScroll.refresh();
			};
			return false;
		} else if ($(evt.target).is(".iconfont") || $(evt.target).is(".fa-arrow-up")) {
			myScroll.scrollTo(0, 0);
			$("#totop").hide();
		};
	})
})