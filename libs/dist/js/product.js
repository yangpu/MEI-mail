document.addEventListener('touchmove', function(e) {
	e.preventDefault();
}, false);
document.ondragstart = function() {
	return false;
}


var proapp = angular.module('proapp', ['commonApp']);
var procontroller = proapp.controller('procontroller', ['$scope', '$http', function($scope, $http) {
	$scope.load = function() {
		var myScroll = new IScroll('#wrapper', {
			scrollbars: true,
			mouseWheel: true,
			interactiveScrollbars: true,
			click:true //允许点击
		});
		window.myScroll = myScroll;
		var opts = {
			flag: false,
			index: 1,
			times: 6, //数据加载的跨度
		}
		myScroll.on('scrollEnd', function() {
			if (this.y <= myScroll.maxScrollY && myScroll.maxScrollY != opts.flag) {
				opts.flag = myScroll.maxScrollY;
				opts.index++;
				$http.get('libs/php/list.php?type=' + opts.index * opts.times).success(function(_response) {
					_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
					$scope.data = _response;
					setTimeout(function() {
						myScroll.refresh();
					}, 1000)
				})
			}
		});
	}
	$http.get('libs/php/list.php?type=' + 6).success(function(_response) {
		_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
		$scope.data = _response;
		setTimeout(function() {
			myScroll.refresh();
		}, 1000)
	})
}])



window.onload = function() {
	//商品加载e
	$(document).on("touchstart", function(evt) {
		// console.log(evt.target);
		if ($(evt.target).is(".goods li")) {
			$(evt.target).css({
					"background-color": "#f2f2f2"
				})
				.siblings().css({
					"background-color": "#eaeaea"
				});
			$(".product li").eq($(evt.target).index()).show().siblings().hide();
		} else if ($(evt.target).is(".icont") || $(evt.target).is(".items")) {
			var $target = null;
			if ($(evt.target).is(".icont")) {
				$target = $(evt.target).closest(".items");
			} else {
				$target = $(evt.target);
			}

			//判断是否为ABC
			if ($target.is(".abc")) {
				$("#wrapper").show().prev().hide();
			} else {
				$(".classify>div").eq($target.index()).show().siblings().hide();
				$("#wrapper").hide();
				$(".classify").show();
			};

			if ($target.find("i").hasClass('fa-caret-down')) {
				$target.find("i").addClass('fa-caret-up').removeClass('fa-caret-down').closest('.items').siblings().find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
				$target.css({
					"color": "pink"
				}).siblings().css({
					"color": "black"
				});
			} else {
				$target.find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
				$target.css({
					"color": "black"
				})
			};
		};
	})
}