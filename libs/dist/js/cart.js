document.addEventListener('touchmove', function(e) {
  e.preventDefault();
}, false);
document.ondragstart = function() {
  return false;
}



var cart = angular.module('cart', ['commonApp']);
var cartcontroller = cart.controller('cartcontroller', ['$scope', '$http', function($scope, $http) {
  $scope.page = "item-hidden";
  $scope.tem = false;
  $scope.all = false; //全选的
  $scope.faclass = "fa-circle-o"; //没有选择的
  $scope.facheck = "fa-check-circle";
  $scope.count = 0; //总数

  $http.post("libs/php/getcart.php", {
    type: "obj",
    number: 6
  }).success(function(_response) {
    _response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;

    if (_response.status) {
      $scope.data = _response.data;
      $scope.tem = false;
    } else {
      alert("购物车为空，请去添加");
      $scope.tem = true;
    }

    setTimeout(function() {
      myScroll.refresh();
    }, 1000)
  })

  //1.加载iscroll
  $scope.load = function() {
    var myScroll = new IScroll('#wrapper', {
      scrollbars: true,
      mouseWheel: true,
      interactiveScrollbars: true,
      click:true 
    });
    window.myScroll = myScroll;

    var opts = {
      flag: false,
      index: 1,
      times: 6, //数据加载的跨度
    }
    myScroll.on('scrollEnd', function() {
      if (this.y <= myScroll.maxScrollY && myScroll.maxScrollY != opts.flag) {
        opts.flag = myScroll.maxScrollY;
        opts.index++;

        $http.post("libs/php/getcart.php", {
          type: "obj",
          number: opts.index * opts.times
        }).success(function(_response) {
          _response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
          if (_response.status) {
            $scope.data = _response.data;
          } else {
            alert("购物车为空，请去添加")
          }
          console.log($scope.data);

          setTimeout(function() {
            myScroll.refresh();
          }, 1000)
        })
      }
    });
  }

  //加减函数的点击事件
  $scope.click = function(value, type) {
    if (type === "plus") {
      value.num = value.num < 100 ? value.num * 1 + 1 : 100;
    } else {
      value.num = value.num > 1 ? value.num * 1 - 1 : 1;
    }
    //计算总数
    $scope.count = $scope.calculate($scope.data);
  }

  //点击是否选中的,添加选中函数
  $scope.each = function(data, boolean) {
    for (var i = 0; i < data.length; i++) {
      data[i].check = boolean;
    }
  }

  //判断全选的函数
  $scope.eachall = function(data) {
    var k = 0;
    for (var i = 0; i < data.length; i++) {
      if (data[i].check) {
        k++;
      }
    }
    if (k == data.length && data.length > 0) {
      return true;
    }
    return false;
  }

  //计算总价函数
  $scope.calculate = function(data) {
      var count = 0
      for (var i = 0; i < data.length; i++) {
        if (data[i].check) {
          count += data[i].productprice * data[i].num;
        }
      }
      return count;
    }
    //下单函数
  $scope.collect = function(data) {
    var array = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i].check) {
        array.push(data[i]);
        //array.push(JSON.stringify(data[i]));
      }
    }
    return array;

  }

  $scope.check = function(val) {
    //对自己
    if (val.check) {
      val.check = false;
    } else {
      val.check = true;
    };
    $scope.all = $scope.eachall($scope.data);
    $scope.count = $scope.calculate($scope.data);
  }

  $scope.allcheck = function() {
    if ($scope.all) {
      $scope.all = false;
      $scope.each($scope.data, false);
    } else {
      $scope.all = true;
      $scope.each($scope.data, true);
    };
    $scope.count = $scope.calculate($scope.data);
  }

  //删除函数
  $scope.remove = function(val) {
    for (var i = 0; i < $scope.data.length; i++) {
      if ($scope.data[i].id == val.id) {
        var idx = i;
      }
    }
    //删除数组里面的obj
    $scope.data.splice(idx, 1);
    alert("删除购物车商品");

    $scope.all = $scope.eachall($scope.data);
    $scope.count = $scope.calculate($scope.data);
    if ($scope.data.length == 0) {
      $scope.tem = true;
    }

    //angular 不能异步
    setTimeout(function() {
      myScroll.refresh();
    }, 200)

    $http.post('libs/php/delcart.php', {
      id: val.id
    }).success(function(_response) {
      console.log(_response);
    })
  }

  $scope.topage = function(val) {
      if (val == "page") {
        $scope.page = "item-block";
      } else {
        $scope.page = "item-hidden";
      }

    }
    //下单函数
  $scope.buy = function() {
    //判断是否登录
    $http.get('libs/php/getsession.php').success(function(_response) {
      _response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
      if (_response.state) {
          var data = $scope.collect($scope.data);
          if (data.length > 0) {
            data = JSON.stringify(data);
            var date = new Date();
            $http.post("libs/php/order.php", {
                orderno: date.getTime()+"",
                product: data,
                count: $scope.count
              }).success(function(_response) {
                _response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
                if (_response.status) {
                  window.location.href = "order.html";
                }else{
                  alert("请重新下单");
                }
              })
          } else {
            alert("请选择商品")
          }
      } else {
          alert("您还没有登录，赶紧去登陆吧")
          window.location.href = "login.html";
          
      }
  })


    



  }
}])