$(document).ajaxSend(function() {
	if ($('.maskmodel')[0]) {
		$('.maskmodel').show();
	} else {
		$('<div class="maskmodel"><i class="fa fa-spinner fa-spin"></i></div>').appendTo('body').show();
	}
});
$(document).ajaxComplete(function() {
	$('.maskmodel').hide();
});

//判断是否已经登录

$.get("libs/php/getsession.php",function(_response){
	_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
	if (_response.state) {
		var n = new Date,
		o = n.getFullYear(),
		p = n.getMonth() + 1,
		a = n.getDate();

		new Vue({
			el:"#nav",
			data:{
				account:_response.account,
				date:o + " / " + p + " / " + a,
			}
		})
	}else{
		window.location.href = "login1.html";
	};
})

/*自定义过滤器*/
Vue.filter("range", function(array, page) {
	for (var i = 0; i < page; i++) {
		array.push(i + 1);
	}
	return array;

});


/* 创建组件构造器  */
/****************************************主页功能组件*******************************************/
/*var Home = Vue.extend({
	template: '#home',
	data: function() {
		return {
			msg: 'Hello, vue router!',
			top: [],
		}
	},
	ready: function() {
		$.get('libs/php/getmodel.php', {
			type: 'tproduct2'
		}, function(_response) {
			_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
			this.top = _response;
			// console.log(_response);
		}.bind(this))


	}
})*/




var Home = Vue.extend({
	template: "#home",
	data: function() {
		return {
			msg: "hello template",
			datas: [],
			pagenumber: 5, //配置每页显示10个数据
			pagecount: 0, //总页数
			pageindex: 1, //默认当前从下标0开始
			revise: false, //是否显示上传框
			productname: "",
			productprice: "",
			indexid: 0,
			filename: "20170127145115_965.jpg",
			exit: false, //true为添加，false为修改,默认为修改。
			upload: false, //默认不上传文件
		}
	},
	ready: function() {
		$.get("libs/php/getmodel.php", {
			type: "tproduct2"
		}, function(_response) {
			_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
			//添加一下数据，选择复制；操作数据前必须要有数据
			for (var i = _response.length - 1; i >= 0; i--) {
				_response[i].check = false;
			};
			console.log(_response);

			this.datas = _response;
			//console.log(_response);
			this.pagecount = Math.ceil(this.datas.length / this.pagenumber); //总页数
			//console.log(this.pagecount)

		}.bind(this));
	},
	methods: {
		//分页事件
		previous: function() {
			if (this.pageindex > 1) {
				this.pageindex--;
			}
			console.log(this.pageindex)
		},
		next: function() {
			if (this.pageindex < this.pagecount) {
				this.pageindex++;
			}
			console.log(this.pageindex)
		},
		page: function(p) {
			this.pageindex = p;
		},

		// 背景颜色
		handleClick: function(obj) {
			obj.check = true;
			var object = this.datas.filter(function(index) {
				return index != obj;
			});
			for (var i = object.length - 1; i >= 0; i--) {
				object[i].check = false;
			};
		},
		//修改选项的事件
		change: function(obj) {
			this.revise = true; //显示上传框
			this.exit = false; //false为修改，当前状态为修改
			this.upload = false; //默认不上传文件
			this.productprice = obj.className;
			this.productname = obj.productname;
			this.indexid = obj.indexid;
			this.filename = obj.filename;
		},
		//关闭按钮
		close: function() {
			this.revise = false;
		},
		//提交事件
		onSubmit: function() {
			if (this.exit) { //上传
				//新增表单的
				$('form').ajaxSubmit({
					type: 'post',
					url: 'libs/php/formmain.php',
					success: function(data) {
						console.log(data);
					},
					error: function(XmlHttpRequest, textStatus, errorThrown) {
						console.log(XmlHttpRequest);
						console.log(textStatus);
						console.log(errorThrown);
					}
				})
			} else {
				//修改产品的
				//判断是否要上传更改图片
				if (this.upload) {
					// console.log("要上传图片")
					$('form').ajaxSubmit({
						type: 'post',
						url: 'libs/php/reviseMainform.php',
						success: function(data) {
							console.log(data);
							window.location.reload();
						},
						error: function(XmlHttpRequest, textStatus, errorThrown) {
							console.log(XmlHttpRequest);
							console.log(textStatus);
							console.log(errorThrown);
						}
					})
				} else {
					$.post("libs/php/reviseMain.php", {
						productname: this.productname,
						productprice: this.productprice,
						indexid: this.indexid,
						type: "revise"
					}, function(_response) {
						_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
						$.alert(_response.message)
						this.revise = false;
						window.location.reload();
					}.bind(this))
				};

			}

		},

		//删除事件
		del: function(obj) {
			if (confirm("确定删除商品")) {
				$.post("libs/php/reviseMain.php", {
					productname: obj.productname,
					productprice: obj.productprice,
					indexid: obj.indexid,
					type: "delete"
				}).success(function(_response) {
					console.log(_response);
				})
			} else {
				console.log("取消了");
			};


		},
		// 添加
		insert: function() {
			this.revise = true; //显示
			this.exit = true; //添加
			this.upload = true; //默认上传
		}

	}
})
/****************************************订单管理功能组件*******************************************/
var Order = Vue.extend({
	template: '#order',
	data: function() {
		return {
			order: []
		}
	},
	ready: function() {
		$.get("libs/php/getmodel.php?type=torder", function(_response) {
			_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
			if (_response.length > 0) {
				for (var i = 0; i < _response.length; i++) {
					_response[i].product = JSON.parse(_response[i].product);
				}
				this.order = _response;
				//console.log(_response);
			} else {
				$.alert("没有订单")
			}
		}.bind(this))
	},
	methods: {
		send: function(obj) {
			if (obj.statue == 0) {
				obj.statue = 1;
				$.get("libs/php/reorder.php", {
					type: "reset",
					orderno: obj.orderno,
					statue: 1
				}, function(_response) {
					console.log(_response)
				})
			} else {
				$.alert("已发货,不要重复点击")
			};
		},
		del: function(obj) {
			if (confirm("是否确定要取消订单")) {
				$.get("libs/php/reorder.php", {
					type: "del",
					orderno: obj.orderno,
					statue: 1
				}, function(_response) {
					console.log(_response)
				})

			} else {
				console.log("取消操作")
			};
		}
	}
})

/****************************************列表功能组件*******************************************/
var List = Vue.extend({
	template: "#list",
	data: function() {
		return {
			msg: "hello template",
			datas: [],
			pagenumber: 5, //配置每页显示10个数据
			pagecount: 0, //总页数
			pageindex: 1, //默认当前从下标0开始
			revise: false, //是否显示上传框
			productname: "",
			productprice: "",
			indexid: 0,
			filename: "20170127153308_136.jpg",
			exit: false, //true为添加，false为修改,默认为修改。
			upload: false, //默认不上传文件
		}
	},
	ready: function() {
		$.get("libs/php/getmodel.php", {
			type: "tlist"
		}, function(_response) {
			_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
			//添加一下数据，选择复制；操作数据前必须要有数据
			for (var i = _response.length - 1; i >= 0; i--) {
				_response[i].check = false;
			};

			this.datas = _response;
			//console.log(_response);
			this.pagecount = Math.ceil(this.datas.length / this.pagenumber); //总页数
			//console.log(this.pagecount)

		}.bind(this));
	},
	methods: {
		//分页事件
		previous: function() {
			if (this.pageindex > 1) {
				this.pageindex--;
			}
			console.log(this.pageindex)
		},
		next: function() {
			if (this.pageindex < this.pagecount) {
				this.pageindex++;
			}
			console.log(this.pageindex)
		},
		page: function(p) {
			this.pageindex = p;
		},

		// 背景颜色
		handleClick: function(obj) {
			obj.check = true;
			var object = this.datas.filter(function(index) {
				return index != obj;
			});
			for (var i = object.length - 1; i >= 0; i--) {
				object[i].check = false;
			};
		},
		//修改选项的事件
		change: function(obj) {
			this.revise = true; //显示上传框
			this.exit = false; //false为修改，当前状态为修改
			this.upload = false; //默认不上传文件
			this.productprice = obj.productprice;
			this.productname = obj.productname;
			this.indexid = obj.indexid;
			this.filename = obj.filename;
		},
		//关闭按钮
		close: function() {
			this.revise = false;
		},
		//提交事件
		onSubmit: function() {
			if (this.exit) { //上传
				//新增表单的
				$('form').ajaxSubmit({
					type: 'post',
					url: 'libs/php/form.php',
					success: function(data) {
						console.log(data);
					},
					error: function(XmlHttpRequest, textStatus, errorThrown) {
						console.log(XmlHttpRequest);
						console.log(textStatus);
						console.log(errorThrown);
					}
				})
			} else {
				//修改产品的
				//判断是否要上传更改图片
				if (this.upload) {
					// console.log("要上传图片")
					$('form').ajaxSubmit({
						type: 'post',
						url: 'libs/php/reviseListform.php',
						success: function(data) {
							console.log(data);
							window.location.reload();
						},
						error: function(XmlHttpRequest, textStatus, errorThrown) {
							console.log(XmlHttpRequest);
							console.log(textStatus);
							console.log(errorThrown);
						}
					})
				} else {
					$.post("libs/php/reviseList.php", {
						productname: this.productname,
						productprice: this.productprice,
						indexid: this.indexid,
						type: "revise"
					}, function(_response) {
						_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
						$.alert(_response.message)
						this.revise = false;
						window.location.reload();
					}.bind(this))
				};

			}

		},

		//删除事件
		del: function(obj) {
			if (confirm("确定删除商品")) {
				$.post("libs/php/reviseList.php", {
					productname: obj.productname,
					productprice: obj.productprice,
					indexid: obj.indexid,
					type: "delete"
				}).success(function(_response) {
					console.log(_response);
				})
			} else {
				console.log("取消了");
			};


		},
		// 添加
		insert: function() {
			this.revise = true; //显示
			this.exit = true; //添加
			this.upload = true; //默认上传
		}

	}
})


/****************************************主页功能组件*******************************************/

/*var main = Vue.extend({
	template: "#main",
	data: function() {
		return {
			main: []
		}
	},
	ready: function() {
		$.get('libs/php/getmodel.php', {
			type: 'tproduct'
		}, function(_response) {
			_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
			this.main = _response;
		}.bind(this))
	}
})*/

var main = Vue.extend({
	template: "#main",
	data: function() {
		return {
			msg: "hello template",
			datas: [],
			pagenumber: 5, //配置每页显示10个数据
			pagecount: 0, //总页数
			pageindex: 1, //默认当前从下标0开始
			revise: false, //是否显示上传框
			productname: "",
			productprice: "",
			indexid: 0,
			filename: "20170127145115_965.jpg",
			exit: false, //true为添加，false为修改,默认为修改。
			upload: false, //默认不上传文件
		}
	},
	ready: function() {
		$.get("libs/php/getmodel.php", {
			type: "tproduct"
		}, function(_response) {
			_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
			//添加一下数据，选择复制；操作数据前必须要有数据
			for (var i = _response.length - 1; i >= 0; i--) {
				_response[i].check = false;
			};

			this.datas = _response;
			//console.log(_response);
			this.pagecount = Math.ceil(this.datas.length / this.pagenumber); //总页数
			//console.log(this.pagecount)

		}.bind(this));
	},
	methods: {
		//分页事件
		previous: function() {
			if (this.pageindex > 1) {
				this.pageindex--;
			}
			console.log(this.pageindex)
		},
		next: function() {
			if (this.pageindex < this.pagecount) {
				this.pageindex++;
			}
			console.log(this.pageindex)
		},
		page: function(p) {
			this.pageindex = p;
		},

		// 背景颜色
		handleClick: function(obj) {
			obj.check = true;
			var object = this.datas.filter(function(index) {
				return index != obj;
			});
			for (var i = object.length - 1; i >= 0; i--) {
				object[i].check = false;
			};
		},
		//修改选项的事件
		change: function(obj) {
			this.revise = true; //显示上传框
			this.exit = false; //false为修改，当前状态为修改
			this.upload = false; //默认不上传文件
			this.productprice = obj.productprice;
			this.productname = obj.productname;
			this.indexid = obj.indexid;
			this.filename = obj.filename;
		},
		//关闭按钮
		close: function() {
			this.revise = false;
		},
		//提交事件
		onSubmit: function() {
			if (this.exit) { //上传
				//新增表单的
				$('form').ajaxSubmit({
					type: 'post',
					url: 'libs/php/formindex.php',
					success: function(data) {
						console.log(data);
					},
					error: function(XmlHttpRequest, textStatus, errorThrown) {
						console.log(XmlHttpRequest);
						console.log(textStatus);
						console.log(errorThrown);
					}
				})
			} else {
				console.log("修改")
				//修改产品的
				//判断是否要上传更改图片
				if (this.upload) {
					// console.log("要上传图片")
					$('form').ajaxSubmit({
						type: 'post',
						url: 'libs/php/reviseIndexform.php',
						success: function(data) {
							console.log(data);
							//window.location.reload();
						},
						error: function(XmlHttpRequest, textStatus, errorThrown) {
							console.log(XmlHttpRequest);
							console.log(textStatus);
							console.log(errorThrown);
						}
					})
				} else {
					$.post("libs/php/reviseIndex.php", {
						productname: this.productname,
						productprice: this.productprice,
						indexid: this.indexid,
						type: "revise"
					}, function(_response) {
						_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
						$.alert(_response.message)
						this.revise = false;
						//window.location.reload();
					}.bind(this))
				};

			}

		},

		//删除事件
		del: function(obj) {
			if (confirm("确定删除商品")) {
				$.post("libs/php/reviseIndex.php", {
					productname: obj.productname,
					productprice: obj.productprice,
					indexid: obj.indexid,
					type: "delete"
				}).success(function(_response) {
					_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
					console.log(_response);
					alert(_response.message);
					 //window.location.reload();
				})
			} else {
				console.log("取消了");
			};


		},
		// 添加
		insert: function() {
			this.revise = true; //显示
			this.exit = true; //添加
			this.upload = true; //默认上传
		}

	}
})



/* 创建路由器  */
var router = new VueRouter()

/* 创建路由映射  */
router.map({
	'/home': {
		component: Home
	},
	'/order': {
		component: Order
	},
	'/List': {
		component: List
	},
	'/main':{
		component:main
	}
})

router.redirect({
	'/': '/home'
})

/* 启动路由  */
var App = Vue.extend({})
router.start(App, '#right')


new Vue({
	el:"#left",
	methods:{
		logOut:function(){
			if (confirm("是否退出系统")) {
				$.get("libs/php/logout.php",function(_response){
					console.log(_response);
					window.location.href = "login1.html";
				})
			};
		}
	}
})