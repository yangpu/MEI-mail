document.addEventListener('touchmove', function(e) {
	e.preventDefault();
}, false);
document.ondragstart = function() {
	return false;
}

var r = $.getUrlParam('id');
if (r == null) {
	window.location.href = "product.html";
}

var delapp = angular.module('delapp', ['commonApp']);
var delcontroller = delapp.controller('delcontroller', ['$scope', '$http', function($scope, $http) {
	$scope.del = "showin";
	$scope.cont = "hidein";
	$scope.delvi = true;
	$scope.contvi = false;

	$http.post('libs/php/detail.php', {
		id: r
	}).success(function(_response) {
		_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
		$scope.data = _response[0];

		setTimeout(function() {
			myScroll.refresh();
		}, 1000)
	})

	$http.post("libs/php/getcart.php",{type:"num"}).success(function(_response){
		$scope.count = _response;
	})

	//1.加载iscroll
	$scope.load = function() {
		var myScroll = new IScroll('#wrapper', {
			scrollbars: true,
			mouseWheel: true,
			interactiveScrollbars: true
		});
		window.myScroll = myScroll;

		setTimeout(function() {
			var mySwiper = new Swiper('.swiper-container', {
				loop: true,
				pagination: '.swiper-pagination',
				paginationClickable: true,
				autoplay: 2000, //可选选项，自动滑动
				autoplayDisableOnInteraction: false,
			})
		}, 500)

		var opts = {
			flag: false,
			index: 1,
			times: 6, //数据加载的跨度
		}
		myScroll.on('scrollEnd', function() {
			if (this.y <= myScroll.maxScrollY && myScroll.maxScrollY != opts.flag) {
				opts.flag = myScroll.maxScrollY;
				opts.index++;
				$http.get('libs/php/detailcom.php?type=' + opts.index * opts.times).success(function(_response) {
					_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
					$scope.comment = _response;
					//console.log($scope.comment)
					setTimeout(function() {
						myScroll.refresh();
					}, 1000)
				})
			}
		});
	}

	

	//2.点击显示事件
	$scope.tog = function(evt) {
		if (evt === "detail") {
			$scope.del = "showin";
			$scope.cont = "hidein";
			$scope.delvi = true;
			$scope.contvi = false;
			myScroll.refresh();
			setTimeout(function() {
				var mySwiper = new Swiper('.swiper-container', {
					loop: true,
					pagination: '.swiper-pagination',
					paginationClickable: true,
					autoplay: 2000, //可选选项，自动滑动
					autoplayDisableOnInteraction: false,
				})
				myScroll.refresh();
			}, 1000)
		} else if (evt === "content") {
			$scope.del = "hidein";
			$scope.cont = "showin";
			$scope.delvi = false;
			$scope.contvi = true;

			$http.get('libs/php/detailcom.php?type=' + 6).success(function(_response) {
				_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
				$scope.comment = _response;
				//console.log($scope.comment)
				setTimeout(function() {
					myScroll.refresh();
				}, 1000)
			})
		}
	}

	//3.加入购物车
	$scope.cart = function() {
		$http.post("libs/php/tocart.php", {
			id: $scope.data.indexid,
			productname: $scope.data.productname,
			productprice: $scope.data.productprice,
			num: 1,
			src:$scope.data.filename
		}).success(function(_response) {
			_response = typeof _response == "string" ? window.eval("(" + _response + ")") : _response;
			alert(_response.message);
			$scope.count = _response.count;
		})
	}
}])

