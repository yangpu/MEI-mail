<?php 
	
	//初始化数据库
	function connect(){
		// $servername   = "localhost";
		// $username     = "root";
		// $userpassword = "root";
		// $dbname       = "sqlserve";

		$servername   = "119.29.99.176";
		$username     = "root";
		$userpassword = "yangpu0924";
		$dbname       = "sqlserve";

		$con = mysqli_connect($servername,$username,$userpassword,$dbname);

		if(mysqli_connect_error($con)){
			echo "连接数据库失败".mysqli_connect_error($con);
			return null;
		}else{
			return $con;
		}
	}

	//执行查询数据方法
	function query($sql){
		//初始化数据库
		$conn = connect();
		$result = mysqli_query($conn,$sql);

		//定义了一个数组
		$jsondata = array();
		if ($result) {
			//在结果集中获取对象(逐行获取)
			while ($obj = mysqli_fetch_array($result)) {
				$jsondata[] = $obj;
			}
			// 释放结果集
			mysqli_free_result($result);
		}
		//关闭连接
		mysqli_close($conn);
		return $jsondata;
	}

	//执行逻辑语句
	function extute($sql){
		$conn = connect();
		$result = mysqli_query($conn,$sql);
		mysqli_close($conn);
		return $result;
	}

	header("Access-Control-Allow-Origin:*");
	/*星号表示所有的域都可以接受，*/
	header("Access-Control-Allow-Methods:GET,POST");

 ?>