-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.5-10.0.14-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 sqlserve 的数据库结构
CREATE DATABASE IF NOT EXISTS `sqlserve` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sqlserve`;


-- 导出  表 sqlserve.register 结构
CREATE TABLE IF NOT EXISTS `register` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  `phone` varchar(50) NOT NULL DEFAULT '0',
  `src` varchar(50) NOT NULL DEFAULT 'libs/dist/image/detail/1.jpg',
  `user` varchar(50) NOT NULL DEFAULT '普通会员',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.register 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `register` DISABLE KEYS */;
INSERT INTO `register` (`indexid`, `account`, `password`, `phone`, `src`, `user`) VALUES
	(1, 'yangpu', '123456', '18312701884', 'libs/dist/image/detail/1.jpg', '普通会员'),
	(2, 'yangpu1', '123456', '13456789098', 'libs/dist/image/detail/1.jpg', '普通会员');
/*!40000 ALTER TABLE `register` ENABLE KEYS */;


-- 导出  表 sqlserve.tcart 结构
CREATE TABLE IF NOT EXISTS `tcart` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL DEFAULT '0',
  `productname` varchar(50) NOT NULL DEFAULT '0',
  `productprice` varchar(50) NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '1',
  `src` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.tcart 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `tcart` DISABLE KEYS */;
INSERT INTO `tcart` (`indexid`, `id`, `productname`, `productprice`, `num`, `src`) VALUES
	(30, 3, 'FIRSTMIX 新品日系高腰宽松流苏边阔腿九分牛仔裤', '1241', 2, '20170214143822_770.jpg');
/*!40000 ALTER TABLE `tcart` ENABLE KEYS */;


-- 导出  表 sqlserve.tdidcuss 结构
CREATE TABLE IF NOT EXISTS `tdidcuss` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(50) NOT NULL DEFAULT 'libs/dist/image/detail/18.jpg',
  `name` varchar(50) NOT NULL DEFAULT '匿名',
  `star` int(11) NOT NULL DEFAULT '4',
  `comtent` varchar(50) NOT NULL DEFAULT '真心好，下次还会再来买',
  `date` varchar(50) NOT NULL DEFAULT '购买日期：2016-05-10 14:00',
  `datetime` varchar(50) NOT NULL DEFAULT '2016-05-16 12:00',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.tdidcuss 的数据：~22 rows (大约)
/*!40000 ALTER TABLE `tdidcuss` DISABLE KEYS */;
INSERT INTO `tdidcuss` (`indexid`, `src`, `name`, `star`, `comtent`, `date`, `datetime`) VALUES
	(1, 'libs/dist/image/detail/18.jpg', '匿名', 4, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(2, 'libs/dist/image/detail/18.jpg', '大强', 4, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(3, 'libs/dist/image/detail/18.jpg', '傻强', 4, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(4, 'libs/dist/image/detail/18.jpg', '傻强', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(5, 'libs/dist/image/detail/18.jpg', '匿名', 5, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(6, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(7, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(8, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(9, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(10, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(11, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(12, 'libs/dist/image/detail/18.jpg', '匿名', 5, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(13, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(14, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(15, 'libs/dist/image/detail/18.jpg', '匿名', 4, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(16, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(17, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(18, 'libs/dist/image/detail/18.jpg', '匿名', 5, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(19, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(20, 'libs/dist/image/detail/18.jpg', '匿名', 1, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(21, 'libs/dist/image/detail/18.jpg', '匿名', 3, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00'),
	(22, 'libs/dist/image/detail/18.jpg', '匿名', 2, '真心好，下次还会再来买', '购买日期：2016-05-10 14:00', '2016-05-16 12:00');
/*!40000 ALTER TABLE `tdidcuss` ENABLE KEYS */;


-- 导出  表 sqlserve.tlist 结构
CREATE TABLE IF NOT EXISTS `tlist` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(50) NOT NULL DEFAULT '0',
  `productprice` float NOT NULL DEFAULT '0',
  `filename` varchar(50) NOT NULL DEFAULT '0',
  `describe` varchar(50) NOT NULL DEFAULT '20170127153828_586.jpg',
  `brand` varchar(50) NOT NULL DEFAULT 'LETDIOSTO',
  `productdes` varchar(50) NOT NULL DEFAULT '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)',
  `pro` varchar(200) NOT NULL DEFAULT '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）',
  `proname` varchar(200) NOT NULL DEFAULT '女款黑白色条纹上衣阔腿裤套装',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.tlist 的数据：~34 rows (大约)
/*!40000 ALTER TABLE `tlist` DISABLE KEYS */;
INSERT INTO `tlist` (`indexid`, `productname`, `productprice`, `filename`, `describe`, `brand`, `productdes`, `pro`, `proname`) VALUES
	(5, '春秋破洞时尚修身牛仔打底裤', 8, '20170214150751_835.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(6, ' 多色牛仔拼接女士半身裙', 149, '20170214150907_895.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(8, 'TOPSHOP 粉色缎面精致动物印花飞行员夹克', 399, '20170214151104_881.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(9, 'FIRSTMIX 秋季新品小脚铆钉开叉直筒牛仔裤', 79, '20170214151239_858.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(10, 'MR&MRS ITALY皮草夫妇浣熊毛滚边貉子毛领派克中长款全棉连帽外套绿色', 9969, '20170214151433_782.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(11, 'LEE 女式牛仔裤LWS403660W27', 299, '20170214151546_780.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(12, 'FIRSTMIX 秋冬款光泽裤加绒加厚中腰显瘦小脚打底裤', 39, '20170214152259_354.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(13, 'MIRACLE KILL/死亡奇迹 烫金小A女款圆领套头卫衣', 229, '20170214152408_467.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(14, 'FIRSTMIX 纯色水洗单排扣显瘦高腰牛仔a字半身裙', 55, '20170214152451_288.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(15, 'LILITH A PARIS 九分袖毛呢外套', 279, '20170214152534_764.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(16, 'LILITH A PARIS 九分袖毛呢外套', 49, '20170214152612_526.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(17, 'FIRSTMIX 高腰八分竖条宽松休闲阔腿裤', 49, '20170214152706_495.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(18, 'ANDREWMARC MNY系列秋冬女士中长款双门襟保暖羽绒服外套', 575, '20170214152818_187.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(19, 'MO&Co.立领趣味老虎图案单侧拉链装饰中长款外套MA153COD08 moco', 1199, '20170214153017_783.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(20, 'LILITH A PARIS 17年春季新款H型九分阔腿裤', 179, '20170214153116_532.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(21, 'MIRACLE KILL/死亡奇迹 背后大圆圈双刀女款连帽套头卫衣', 249, '20170214153330_869.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(22, 'FIRSTMIX2017春款侧边拉链高腰牛仔裙', 85, '20170214153456_654.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(23, 'MO&Co.小高领针织拼梭织纯色褶裥短袖连衣裙伞裙MA1631DRS21 moco', 979, '20170214153547_722.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(24, 'LILITH A PARIS H型前短后长开叉雪纺衫', 169, '20170214154739_799.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(26, 'LILITH A PARIS 中长款经典风衣', 299, '20170213150905_401.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(27, 'LILITH A PARIS 蝴蝶结宽松纯色雪纺衫长袖衬衫', 99, '20170213153440_532.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(29, '汉唐流线裙', 1998, '20170213174955_360.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(30, '黑色高腰小短裤', 227, '20170214142346_682.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(31, 'LILITH A PARIS  欧根纱荷叶边翻领七分袖连衣裙', 289, '20170214142526_811.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(32, 'ANDERSSON BELL女式维罗妮卡双排扣大衣 ', 1099, '20170214142618_334.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(33, 'LILITH A PARIS 中长款修身五分袖连衣裙', 109, '20170214142714_680.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(34, 'MIRACLE KILL/死亡奇迹 烫金小A轮廓女款连帽套头卫衣', 199, '20170214143050_470.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(35, 'LILITH A PARIS H型宽松约克风衣', 339, '20170214154834_638.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(36, 'MO&Co.宽背带口袋长款宽松纯色连体裤女复古MA1631JPS02 moco', 399, '20170214154920_727.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(37, 'LILITH A PARIS 一字领露肩挽袖衬衫', 159, '20170214160816_328.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装'),
	(38, 'LILITH A PARIS 17年 春季新款中长款单排扣风衣', 339, '20170214160944_550.jpg', '20170127153828_586.jpg', 'LETDIOSTO', '上衣:粘纤77% 聚酯纤维18.5% 氨纶4.5% 裤子:聚酯纤维100% (装饰物除外)', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗（深色或者深色与浅色相拼的衣服洗涤时请注意：不宜用碱性过强的洗涤液；不宜浸泡时间过久）', '女款黑白色条纹上衣阔腿裤套装');
/*!40000 ALTER TABLE `tlist` ENABLE KEYS */;


-- 导出  表 sqlserve.torder 结构
CREATE TABLE IF NOT EXISTS `torder` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `orderno` varchar(50) NOT NULL DEFAULT '0',
  `product` varchar(1000) NOT NULL,
  `count` float NOT NULL,
  `statue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.torder 的数据：~5 rows (大约)
/*!40000 ALTER TABLE `torder` DISABLE KEYS */;
INSERT INTO `torder` (`indexid`, `orderno`, `product`, `count`, `statue`) VALUES
	(6, '1486103779417', '[{"0":"25","1":"1","2":"欧莱雅羊毛连衣裙","3":"2241","4":"1","5":"20170127153308_136.jpg","indexid":"25","id":"1","productname":"欧莱雅羊毛连衣裙","productprice":"2241","num":"1","src":"20170127153308_136.jpg","$$hashKey":"object:6","check":true},{"0":"26","1":"3","2":"GLY国际model连衣裙","3":"1241","4":"1","5":"20170127153412_868.jpg","indexid":"26","id":"3","productname":"GLY国际model连衣裙","productprice":"1241","num":"1","src":"20170127153412_868.jpg","$$hashKey":"object:7","check":true},{"0":"27","1":"26","2":"欧莱雅国际连衣裙","3":"1249","4":"1","5":"20170127153522_469.jpg","indexid":"27","id":"26","productname":"欧莱雅国际连衣裙","productprice":"1249","num":"1","src":"20170127153522_469.jpg","$$hashKey":"object:8","check":true}]', 4731, 1),
	(7, '1486361530311', '[{"0":"25","1":"1","2":"欧莱雅羊毛连衣裙","3":"2241","4":"1","5":"20170127153308_136.jpg","indexid":"25","id":"1","productname":"欧莱雅羊毛连衣裙","productprice":"2241","num":9,"src":"20170127153308_136.jpg","$$hashKey":"object:6","check":true},{"0":"26","1":"3","2":"GLY国际model连衣裙","3":"1241","4":"1","5":"20170127153412_868.jpg","indexid":"26","id":"3","productname":"GLY国际model连衣裙","productprice":"1241","num":9,"src":"20170127153412_868.jpg","$$hashKey":"object:7","check":true},{"0":"27","1":"26","2":"欧莱雅国际连衣裙","3":"1249","4":"1","5":"20170127153522_469.jpg","indexid":"27","id":"26","productname":"欧莱雅国际连衣裙","productprice":"1249","num":18,"src":"20170127153522_469.jpg","$$hashKey":"object:8","check":true}]', 53820, 0),
	(8, '1487145958217', '[{"0":"26","1":"2","2":"FIRSTMIX 复古T恤连衣裙侧边开叉休闲长裙","3":"55","4":"1","5":"20170214143554_936.jpg","indexid":"26","id":"2","productname":"FIRSTMIX 复古T恤连衣裙侧边开叉休闲长裙","productprice":"55","num":"1","src":"20170214143554_936.jpg","$$hashKey":"object:6","check":true},{"0":"27","1":"3","2":"FIRSTMIX 新品日系高腰宽松流苏边阔腿九分牛仔裤","3":"1241","4":"2","5":"20170214143822_770.jpg","indexid":"27","id":"3","productname":"FIRSTMIX 新品日系高腰宽松流苏边阔腿九分牛仔裤","productprice":"1241","num":"2","src":"20170214143822_770.jpg","$$hashKey":"object:7","check":true},{"0":"28","1":"4","2":"FIRSTMIX 学院风破洞牛仔背带裤","3":"69","4":"1","5":"20170214143959_954.jpg","indexid":"28","id":"4","productname":"FIRSTMIX 学院风破洞牛仔背带裤","productprice":"69","num":"1","src":"20170214143959_954.jpg","$$hashKey":"object:8","check":true}]', 2606, 0),
	(9, '1487217575987', '[{"0":"26","1":"2","2":"FIRSTMIX 复古T恤连衣裙侧边开叉休闲长裙","3":"55","4":"1","5":"20170214143554_936.jpg","indexid":"26","id":"2","productname":"FIRSTMIX 复古T恤连衣裙侧边开叉休闲长裙","productprice":"55","num":"1","src":"20170214143554_936.jpg","$$hashKey":"object:6","check":true},{"0":"27","1":"3","2":"FIRSTMIX 新品日系高腰宽松流苏边阔腿九分牛仔裤","3":"1241","4":"2","5":"20170214143822_770.jpg","indexid":"27","id":"3","productname":"FIRSTMIX 新品日系高腰宽松流苏边阔腿九分牛仔裤","productprice":"1241","num":"2","src":"20170214143822_770.jpg","$$hashKey":"object:7","check":true},{"0":"28","1":"4","2":"FIRSTMIX 学院风破洞牛仔背带裤","3":"69","4":"1","5":"20170214143959_954.jpg","indexid":"28","id":"4","productname":"FIRSTMIX 学院风破洞牛仔背带裤","productprice":"69","num":"1","src":"20170214143959_954.jpg","$$hashKey":"object:8","check":true}]', 2606, 1),
	(10, '1487416067870', '[{"0":"27","1":"3","2":"FIRSTMIX 新品日系高腰宽松流苏边阔腿九分牛仔裤","3":"1241","4":"2","5":"20170214143822_770.jpg","indexid":"27","id":"3","productname":"FIRSTMIX 新品日系高腰宽松流苏边阔腿九分牛仔裤","productprice":"1241","num":"2","src":"20170214143822_770.jpg","$$hashKey":"object:6","check":true},{"0":"29","1":"2","2":"FIRSTMIX 复古T恤连衣裙侧边开叉休闲长裙","3":"55","4":"2","5":"20170214143554_936.jpg","indexid":"29","id":"2","productname":"FIRSTMIX 复古T恤连衣裙侧边开叉休闲长裙","productprice":"55","num":"2","src":"20170214143554_936.jpg","$$hashKey":"object:7","check":true}]', 2592, 0);
/*!40000 ALTER TABLE `torder` ENABLE KEYS */;


-- 导出  表 sqlserve.tproduct 结构
CREATE TABLE IF NOT EXISTS `tproduct` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(500) DEFAULT NULL,
  `productprice` float DEFAULT '0',
  `nopage` varchar(50) DEFAULT NULL,
  `filename` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.tproduct 的数据：~29 rows (大约)
/*!40000 ALTER TABLE `tproduct` DISABLE KEYS */;
INSERT INTO `tproduct` (`indexid`, `productname`, `productprice`, `nopage`, `filename`) VALUES
	(30, '控油祛痘精华套装', 225, '0', '20170127145656_846.jpg'),
	(31, ' 拱辰香雪精华套装2ddddd', 345, '0', '20170127145251_171.jpg'),
	(32, '理肤泉精华套装', 234, '0', '20170127145410_896.jpg'),
	(33, '美宝莲精华套装', 124, '0', '20170127145602_291.jpg'),
	(34, '百事达精华套装', 226, '0', '20170127145739_264.jpg'),
	(35, '香水精华套装', 227, '0', '20170127145849_113.jpg'),
	(36, '资源精华套装', 278, '0', '20170127145926_857.jpg'),
	(37, '奶粉精华套装', 289, '0', '20170127150035_910.jpg'),
	(38, '奶粉精华套装', 289, '0', '20170127150041_158.jpg'),
	(39, ' 拱辰香雪精华套装', 345, '0', '20170127145115_965.jpg'),
	(40, '理肤泉精华套装', 234, '0', '20170127145410_896.jpg'),
	(41, ' 拱辰香雪精华套装2', 345, '0', '20170127145251_171.jpg'),
	(42, '美宝莲精华套装', 124, '0', '20170127145602_291.jpg'),
	(43, '控油祛痘精华套装', 225, '0', '20170127145656_846.jpg'),
	(44, '香水精华套装', 227, '0', '20170127145849_113.jpg'),
	(45, '百事达精华套装', 226, '0', '20170127145739_264.jpg'),
	(46, '奶粉精华套装', 289, '0', '20170127150035_910.jpg'),
	(47, '资源精华套装', 278, '0', '20170127145926_857.jpg'),
	(48, '海飞丝', 789, '0', '20170127150136_251.jpg'),
	(49, '控油祛痘精华套装', 225, '0', '20170127145656_846.jpg'),
	(50, ' 拱辰香雪精华套装2', 345, '0', '20170127145251_171.jpg'),
	(51, '理肤泉精华套装', 234, '0', '20170127145410_896.jpg'),
	(52, '美宝莲精华套装', 124, '0', '20170127145602_291.jpg'),
	(53, '百事达精华套装', 226, '0', '20170127145739_264.jpg'),
	(54, '香水精华套装', 227, '0', '20170127145849_113.jpg'),
	(55, '资源精华套装', 278, '0', '20170127145926_857.jpg'),
	(56, '奶粉精华套装', 289, '0', '20170127150035_910.jpg'),
	(57, '奶粉精华套装', 289, '0', '20170216120031_989.jpg'),
	(58, '天天特价1', 345, NULL, '20170216104238_159.jpg');
/*!40000 ALTER TABLE `tproduct` ENABLE KEYS */;


-- 导出  表 sqlserve.tproduct2 结构
CREATE TABLE IF NOT EXISTS `tproduct2` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(50) DEFAULT NULL,
  `filename` varchar(100) NOT NULL DEFAULT '0',
  `className` varchar(100) NOT NULL DEFAULT 'col-xs-12',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.tproduct2 的数据：~22 rows (大约)
/*!40000 ALTER TABLE `tproduct2` DISABLE KEYS */;
INSERT INTO `tproduct2` (`indexid`, `productname`, `filename`, `className`) VALUES
	(13, '男装11111111111111', '20170127151807_427.jpg', 'col-xs-6'),
	(14, '母婴童装', '20170127151828_719.jpg', 'col-xs-6'),
	(15, '美妆', '20170127151843_961.jpg', 'col-xs-6'),
	(16, '鞋包', '20170127151857_349.jpg', 'col-xs-6'),
	(18, '运动户外', '20170127151927_138.jpg', 'col-xs-6'),
	(19, '家电数码', '20170127151951_240.jpg', 'col-xs-6'),
	(20, '食品盛宴', '20170127152009_961.jpg', 'col-xs-6'),
	(21, '手表配饰', '20170127152031_835.jpg', 'col-xs-6'),
	(22, '家具家纺', '20170127152101_889.jpg', 'col-xs-6'),
	(23, '海外尖货', '20170127152136_982.jpg', 'col-xs-6'),
	(24, '全场特惠', '20170127152156_743.jpg', 'col-xs-12'),
	(25, '情人节小心', '20170219135213_763.jpg', 'col-xs-12'),
	(26, '今日特惠', '20170219135235_236.jpg', 'col-xs-12'),
	(27, '靓妹来勾搭', '20170219135251_871.jpg', 'col-xs-12'),
	(28, '别光追剧，看别人怎么穿', '20170219135304_190.jpg', 'col-xs-12'),
	(29, '精华套装表头', '20170127151753_227.jpg', 'col-xs-12'),
	(31, '11111', '20170216114930_266.jpg', 'col-xs-12'),
	(33, '靓妹来勾搭', '20170219140948_641.jpeg', 'col-xs-12'),
	(34, '靓妹来勾搭', '20170216115358_230.jpg', 'col-xs-12'),
	(35, '靓妹来勾搭', '20170216115408_266.jpg', 'col-xs-12');
/*!40000 ALTER TABLE `tproduct2` ENABLE KEYS */;


-- 导出  表 sqlserve.tuser1 结构
CREATE TABLE IF NOT EXISTS `tuser1` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `grade` varchar(50) NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.tuser1 的数据：~6 rows (大约)
/*!40000 ALTER TABLE `tuser1` DISABLE KEYS */;
INSERT INTO `tuser1` (`indexid`, `grade`, `num`) VALUES
	(1, '一级会员', 0),
	(2, '二级会员', 0),
	(3, '三级会员', 0),
	(4, '升级为商家', 0),
	(5, '完善资料', 0),
	(6, '我的订单', 0);
/*!40000 ALTER TABLE `tuser1` ENABLE KEYS */;


-- 导出  表 sqlserve.tuser2 结构
CREATE TABLE IF NOT EXISTS `tuser2` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 正在导出表  sqlserve.tuser2 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `tuser2` DISABLE KEYS */;
INSERT INTO `tuser2` (`indexid`, `name`, `number`) VALUES
	(1, '我的金额', 100),
	(2, '可用金额', 0),
	(3, '代币', 0);
/*!40000 ALTER TABLE `tuser2` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
