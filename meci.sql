-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.5-10.0.14-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 gz1609 的数据库结构
CREATE DATABASE IF NOT EXISTS `gz1609` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gz1609`;


-- 导出  表 gz1609.comment 结构
CREATE TABLE IF NOT EXISTS `comment` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `userimg` varchar(200) DEFAULT NULL,
  `username` varchar(200) NOT NULL,
  `time` datetime NOT NULL,
  `star` varchar(200) NOT NULL,
  `content` varchar(200) NOT NULL,
  `buytime` datetime NOT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.comment 的数据：~10 rows (大约)
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` (`indexid`, `userimg`, `username`, `time`, `star`, `content`, `buytime`) VALUES
	(1, 'libs/images/56.png', '匿名', '2017-02-01 14:12:23', 'libs/images/star5.png', '真心好，下次还会再来买的', '2017-02-01 14:13:14'),
	(2, 'libs/images/56.png', '晓萤', '2017-02-16 14:16:23', 'libs/images/star5.png', '很喜欢，下次还会再来买的。', '2017-02-09 10:16:53'),
	(3, 'libs/images/56.png', '叮当', '2017-02-02 12:17:34', 'libs/images/star4.png', '还不错。', '2017-01-26 14:18:11'),
	(4, 'libs/images/56.png', '小明', '2017-02-15 16:18:57', 'libs/images/star4.png', '还不错。', '2017-02-10 12:19:19'),
	(5, 'libs/images/56.png', '多多', '2017-02-09 14:20:37', 'libs/images/star5.png', '真心好，下次还会再来买的。', '2017-01-05 09:19:47'),
	(6, 'libs/images/56.png', '匿名', '2016-12-22 14:21:15', 'libs/images/star3.png', '一般吧', '2016-12-19 18:21:51'),
	(7, 'libs/images/56.png', '匿名', '2017-01-20 16:23:24', 'libs/images/star5.png', '真不错，下次还会再来的。', '2017-01-23 21:22:18'),
	(8, 'libs/images/56.png', '匿名', '2017-02-16 13:24:10', 'libs/images/star5.png', '真不错，下次还会再来的。', '2017-02-12 14:24:47'),
	(9, 'libs/images/56.png', '匿名', '2017-02-08 14:25:35', 'libs/images/star5.png', '真不错，下次还会再来的。', '2017-02-15 16:24:58'),
	(10, 'libs/images/56.png', '匿名', '2017-02-09 07:25:57', 'libs/images/star5.png', '真不错，下次还会再来的。', '2017-02-03 14:26:24');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;


-- 导出  表 gz1609.consumers 结构
CREATE TABLE IF NOT EXISTS `consumers` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `rephone` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.consumers 的数据：~7 rows (大约)
/*!40000 ALTER TABLE `consumers` DISABLE KEYS */;
INSERT INTO `consumers` (`indexid`, `username`, `phone`, `password`, `rephone`) VALUES
	(1, 'aluo', '18200000000', 'aaaaaa', '11111111111'),
	(2, 'xjl', '18800000000', '111111', '12121212121'),
	(3, 'Jane', '15200000000', '000000', '00000000000'),
	(4, 'eno', '13122222222', '222222', '12345678909'),
	(5, '漂洋', '18611111111', '111111', '11111111111'),
	(6, 'lan', '13511111111', '111111', '11111111111'),
	(8, 'xie', '13500000000', '111111', 'undefined');
/*!40000 ALTER TABLE `consumers` ENABLE KEYS */;


-- 导出  表 gz1609.indexproduct 结构
CREATE TABLE IF NOT EXISTS `indexproduct` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `productname` varchar(200) NOT NULL,
  `price` varchar(20) NOT NULL,
  `filepath` varchar(200) NOT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.indexproduct 的数据：~30 rows (大约)
/*!40000 ALTER TABLE `indexproduct` DISABLE KEYS */;
INSERT INTO `indexproduct` (`indexid`, `productname`, `price`, `filepath`) VALUES
	(1, '拱辰享雪美白精华套装', '120', 'libs/images/5.jpg'),
	(2, '津率享红华凝香平衡液 150ml', '120', 'libs/images/6.jpg'),
	(3, '拱辰享雪美白精华套装', '120', 'libs/images/7.jpg'),
	(4, '拱辰享雪美白精华套装', '120', 'libs/images/8.jpg'),
	(5, '拱辰享雪美白精华套装', '120', 'libs/images/9.jpg'),
	(6, '拱辰享雪美白精华套装', '120', 'libs/images/10.jpg'),
	(7, '拱辰享雪美白精华套装', '120', 'libs/images/1.jpg'),
	(8, '拱辰享雪美白精华套装', '120', 'libs/images/2.jpg'),
	(9, '拱辰享雪美白精华套装', '120', 'libs/images/3.jpg'),
	(10, '拱辰享雪美白精华套装', '120', 'libs/images/4.jpg'),
	(11, '拱辰享雪美白精华套装', '120', 'libs/images/5.jpg'),
	(12, '拱辰享雪美白精华套装', '120', 'libs/images/6.jpg'),
	(13, '拱辰享雪美白精华套装', '120', 'libs/images/7.jpg'),
	(14, '拱辰享雪美白精华套装', '120', 'libs/images/8.jpg'),
	(15, '拱辰享雪美白精华套装', '120', 'libs/images/9.jpg'),
	(16, '拱辰享雪美白精华套装', '120', 'libs/images/10.jpg'),
	(17, '拱辰享雪美白精华套装', '120', 'libs/images/1.jpg'),
	(18, '拱辰享雪美白精华套装', '120', 'libs/images/2.jpg'),
	(19, '拱辰享雪美白精华套装', '120', 'libs/images/3.jpg'),
	(20, '拱辰享雪美白精华套装', '120', 'libs/images/4.jpg'),
	(21, '拱辰享雪美白精华套装', '120', 'libs/images/5.jpg'),
	(22, '拱辰享雪美白精华套装', '120', 'libs/images/6.jpg'),
	(23, '拱辰享雪美白精华套装', '120', 'libs/images/7.jpg'),
	(24, '拱辰享雪美白精华套装', '120', 'libs/images/8.jpg'),
	(25, '拱辰享雪美白精华套装', '120', 'libs/images/9.jpg'),
	(26, '拱辰享雪美白精华套装', '120', 'libs/images/10.jpg'),
	(27, '拱辰享雪美白精华套装', '120', 'libs/images/1.jpg'),
	(28, '拱辰享雪美白精华套装', '120', 'libs/images/2.jpg'),
	(29, '拱辰享雪美白精华套装', '120', 'libs/images/3.jpg'),
	(30, '拱辰享雪美白精华套装', '120', 'libs/images/4.jpg');
/*!40000 ALTER TABLE `indexproduct` ENABLE KEYS */;


-- 导出  表 gz1609.orderlist 结构
CREATE TABLE IF NOT EXISTS `orderlist` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `orderno` varchar(200) NOT NULL COMMENT '订单编号',
  `products` varchar(200) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `orderstate` varchar(1) NOT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.orderlist 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `orderlist` DISABLE KEYS */;
INSERT INTO `orderlist` (`indexid`, `orderno`, `products`, `phone`, `orderstate`) VALUES
	(6, '1487588991265', '[{"goodsno":"010011","count":"3"},{"goodsno":"010013","count":"2"}]', '15200000000', '0'),
	(7, '1487594387986', '[{"goodsno":"010011","count":"3"}]', '15200000000', '0'),
	(8, '1487594397048', '[{"goodsno":"010013","count":"2"}]', '15200000000', '0');
/*!40000 ALTER TABLE `orderlist` ENABLE KEYS */;


-- 导出  表 gz1609.product 结构
CREATE TABLE IF NOT EXISTS `product` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `goodsno` varchar(20) NOT NULL COMMENT '商品编号',
  `goodsname` varchar(200) NOT NULL COMMENT '商品名称',
  `goodsbrand` varchar(50) NOT NULL COMMENT '商品品牌',
  `price` varchar(20) NOT NULL COMMENT '商品价格',
  `src` varchar(200) DEFAULT NULL COMMENT '商品图片',
  `amount` int(6) NOT NULL DEFAULT '1' COMMENT '商品数量',
  `goodsarea` varchar(200) DEFAULT NULL COMMENT '商品产地',
  `material` varchar(200) DEFAULT NULL COMMENT '商品材质',
  `illustration` varchar(200) DEFAULT NULL COMMENT '说明',
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.product 的数据：~6 rows (大约)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`indexid`, `goodsno`, `goodsname`, `goodsbrand`, `price`, `src`, `amount`, `goodsarea`, `material`, `illustration`) VALUES
	(10, '010010', '羊毛混纺长袖毛衫10', 'Versace', '120', 'libs/images/d1.jpg', 100, '中国', '粘纤77%    聚酯纤维18.5%    氨纶4.5%', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗'),
	(11, '010011', '羊毛混纺长袖毛衫11', 'Versace', '120', 'libs/images/d1.jpg', 10, '中国', '粘纤77%    聚酯纤维18.5%    氨纶4.5%', '最高洗涤温度40℃ 不可漂白 平摊晾干 熨斗底板最高温度110℃ 常规干洗'),
	(13, '010012', '棉质儿童长袖衫', 'Gucci', '200', 'libs/images/d1.jpg', 80, '中国', '全棉', '最高洗涤温度40℃ 不可漂白 平摊晾干'),
	(14, '010013', '儿童连帽卫衣', 'Gucci', '220', 'libs/images/d1.jpg', 75, '中国', '全棉', '最高洗涤温度40℃ 熨斗底板最高温度110℃ 常规干洗'),
	(15, '010014', '儿童连帽卫衣2', 'Gucci', '100', 'libs/images/d1.jpg', 30, '中国', '全棉', '最高洗涤温度40℃ 熨斗底板最高温度110℃ 常规干洗'),
	(16, '010015', '儿童连帽卫衣3', 'Gucci', '300', 'libs/images/d1.jpg', 100, '中国', '全棉', '最高洗涤温度40℃ 熨斗底板最高温度110℃ 常规干洗');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;


-- 导出  表 gz1609.recommendpro 结构
CREATE TABLE IF NOT EXISTS `recommendpro` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `productname` varchar(200) NOT NULL,
  `filepath` varchar(200) NOT NULL,
  `class` varchar(200) NOT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.recommendpro 的数据：~16 rows (大约)
/*!40000 ALTER TABLE `recommendpro` DISABLE KEYS */;
INSERT INTO `recommendpro` (`indexid`, `productname`, `filepath`, `class`) VALUES
	(1, '精选品类', 'libs/images/49.jpg', 'col-xs-12'),
	(2, '女装', 'libs/images/31.jpg', 'col-xs-12'),
	(3, '男装', 'libs/images/27.jpg', 'col-xs-6'),
	(4, '母婴童装', 'libs/images/28.jpg', 'col-xs-6'),
	(5, '美妆', 'libs/images/29.jpg', 'col-xs-6'),
	(6, '鞋包', 'libs/images/30.jpg', 'col-xs-6'),
	(7, '运动户外', 'libs/images/32.jpg', 'col-xs-6'),
	(8, '家电数码', 'libs/images/33.jpg', 'col-xs-6'),
	(9, '食品盛宴', 'libs/images/34.jpg', 'col-xs-6'),
	(10, '手表配饰', 'libs/images/35.jpg', 'col-xs-6'),
	(11, '家居家纺', 'libs/images/36.jpg', 'col-xs-6'),
	(12, '海外尖货', 'libs/images/37.jpg', 'col-xs-6'),
	(13, '状出迷人美肌', 'libs/images/50.jpg', 'col-xs-12'),
	(14, '今日特惠大牌', 'libs/images/51.jpg', 'col-xs-12'),
	(15, '穿搭不浮夸', 'libs/images/52.jpg', 'col-xs-12'),
	(16, '小可爱', 'libs/images/53.jpg', 'col-xs-12');
/*!40000 ALTER TABLE `recommendpro` ENABLE KEYS */;


-- 导出  表 gz1609.shoppingcart 结构
CREATE TABLE IF NOT EXISTS `shoppingcart` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `goodsno` varchar(20) NOT NULL,
  `goodsname` varchar(200) NOT NULL,
  `src` varchar(200) DEFAULT NULL,
  `price` varchar(20) NOT NULL,
  `count` int(6) NOT NULL,
  `phone` varchar(11) NOT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.shoppingcart 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
INSERT INTO `shoppingcart` (`indexid`, `goodsno`, `goodsname`, `src`, `price`, `count`, `phone`) VALUES
	(3, '010005', '羊毛混纺长袖毛衫5', 'libs/images/d1.jpg', '120', 2, '18200000000'),
	(5, '010011', '羊毛混纺长袖毛衫11', 'libs/images/d1.jpg', '120', 3, '15200000000'),
	(7, '010013', '儿童连帽卫衣', 'libs/images/20170216100701_574.jpg', '220', 2, '15200000000');
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;


-- 导出  表 gz1609.tmodel 结构
CREATE TABLE IF NOT EXISTS `tmodel` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `modelname` varchar(50) NOT NULL,
  `path` varchar(200) NOT NULL,
  `createtime` datetime DEFAULT NULL,
  `createuser` varchar(50) DEFAULT 'lan',
  `updatetime` datetime DEFAULT NULL,
  `updateuser` varchar(50) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.tmodel 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `tmodel` DISABLE KEYS */;
INSERT INTO `tmodel` (`id`, `modelname`, `path`, `createtime`, `createuser`, `updatetime`, `updateuser`) VALUES
	(1, '商品管理', 'product1.html', '2017-02-06 14:00:00', 'lan', NULL, ''),
	(4, '修改密码', 'resetpassword.html', '2017-02-06 19:58:24', 'lan', NULL, ''),
	(5, '退出登录', 'php/logout.php', '2017-02-06 19:59:08', 'lan', NULL, '');
/*!40000 ALTER TABLE `tmodel` ENABLE KEYS */;


-- 导出  表 gz1609.users 结构
CREATE TABLE IF NOT EXISTS `users` (
  `indexid` int(9) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 正在导出表  gz1609.users 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`indexid`, `account`, `password`, `phone`, `mail`, `username`) VALUES
	(1, 'xjl', '111111', '18872220502', 'xie@xie.com', 'xie'),
	(2, 'abc', '111111', '18872220502', '123@139.com', 'aluo');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
